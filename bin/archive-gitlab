#!/usr/bin/env bash
#
# Archived project(s) (aka package repositories) on GitLab.com
#   _Open MR one day for devscripts salsa_
#
# Usage: $ ./bin/archive-gitlab <project1> [<project2>]
#
# Example:
#   $ ./bin/archive-gitlab project1 project2
#   $ SALSA_TOKEN=xxx ./bin/archive-gitlab project
#
# Requirements:
#   # apt update && apt install --yes --no-install-recommends curl ca-certificates
#   - GitLab API token: https://gitlab.com/-/profile/personal_access_tokens
#

## Quit on error
set -e

packages=$*

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

check_return_code() {
  code=$?
  if [ ${code} -eq 0 ]; then
    echo "[+] ${package} archived ~ https://gitlab.com/kalilinux/packages/${package}"
  else
    echo "[-] ERROR: Something went wrong with archival of ${package}: ${code} ~ https://gitlab.com/kalilinux/packages/${package}" >&2
  fi
}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

source .gitlab-token 2>/dev/null \
  || true

if [ -z "${SALSA_TOKEN}" ]; then
  echo "[-] ERROR: You are missing: $( pwd )/.gitlab-token" >&2
  echo "    Create: https://gitlab.com/-/profile/personal_access_tokens" >&2
  echo "    Afterwards: echo \"SALSA_TOKEN='XXX'\" > $( pwd )/.gitlab-token" >&2
  exit 1
fi

if [ -z "${packages}" ]; then
  echo "[-] ERROR: No project(s) indicated on the command line" >&2
  echo "    $0 <project1> [<project2>]" >&2
  exit 1
fi

# Check if curl is installed
if ! command -v curl 1>/dev/null 2>&1; then
  echo "[-] Please run: $ sudo apt install curl" >&2
  exit 1
fi

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

for package in ${packages}; do
  curl \
    --silent \
    --show-error \
    --fail \
    --retry 5 \
    --header "PRIVATE-TOKEN: ${SALSA_TOKEN}" \
    --request POST \
    --output /dev/null \
    "https://gitlab.com/api/v4/projects/kalilinux%2Fpackages%2F${package}/archive"

  check_return_code
done

echo "[+] Done"
